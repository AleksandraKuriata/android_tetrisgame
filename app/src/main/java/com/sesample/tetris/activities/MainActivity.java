package com.sesample.tetris.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sesample.tetris.R;
import com.sesample.tetris.game.Game;

public class MainActivity extends Activity implements OnClickListener {

    //label dla okreslenia danych intentu
    public static final String EXTRA_LEVEL = "com.sesample.tetris.EXTRA_LEVEL";

    private GameSurfaceView mGameSurfaceView;
    private ImageButton mLeft;
    private ImageButton mRight;
    private ImageButton mDown;
    private ImageButton mTurn;

    private int mScore;
    private TextView mScoreView;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        mGameSurfaceView = (GameSurfaceView) findViewById(R.id.game);

        mLeft = (ImageButton) findViewById(R.id.left);
        mLeft.setOnClickListener(this);

        mRight = (ImageButton) findViewById(R.id.right);
        mRight.setOnClickListener(this);

        mDown = (ImageButton) findViewById(R.id.down);
        mDown.setOnClickListener(this);

        mTurn = (ImageButton) findViewById(R.id.turn);
        mTurn.setOnClickListener(this);

        int level = getIntent().getIntExtra(EXTRA_LEVEL, Game.LEVEL_NORMAL);
        TextView levelTextView = (TextView) findViewById(R.id.level);

        final String[] levelNames = {
                "EASY",
                "NORMAL",
                "HARD",
        };
        levelTextView.setText(levelNames[level]);
        mGameSurfaceView.getRenderer().getGame().setLevel(level);

        mGameSurfaceView.setOnClickListener(this);

        mScore = 0;
        mScoreView = (TextView) findViewById(R.id.score);
        Game.OnScoreChangedListener listener = new Game.OnScoreChangedListener() {
            @Override
            public void onScoreChanged(final int score) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String toastMessage = null;
                        if (score == 1 && mScore == 0) {
                            toastMessage = "1 point!";
                        } else if (score == 5 && mScore == 4) {
                            toastMessage = "5 points!";
                        }
                        if (toastMessage != null) {
                            Toast.makeText(MainActivity.this, toastMessage, Toast.LENGTH_SHORT).show();
                        }
                        mScore = score;
                        mScoreView.setText(Integer.toString(mScore));
                    }
                });
            }
        };
        mGameSurfaceView.getRenderer().getGame().setOnScoreChangedListener(listener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGameSurfaceView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mGameSurfaceView.onPause();
    }

    @Override
    public void onClick(View v) {
        int command = 0;
        switch (v.getId()) {
            case R.id.left:
                command = Game.COMMAND_LEFT;
                break;
            case R.id.right:
                command = Game.COMMAND_RIGHT;
                break;
            case R.id.turn:
                command = Game.COMMAND_TURN;
                break;
            case R.id.down:
                command = Game.COMMAND_DOWN;
                break;
            case R.id.game:
                if (mGameSurfaceView.getRenderer().getGame().isGameOver()) {
                    Intent i = new Intent(MainActivity.this, ResultActivity.class);
                    i.putExtra(ResultActivity.EXTRA_SCORE, mScore);
                    startActivity(i);
                    finish();
                }
                break;
            default:
                break;
        }
        mGameSurfaceView.sendCommand(command);
    }
}
