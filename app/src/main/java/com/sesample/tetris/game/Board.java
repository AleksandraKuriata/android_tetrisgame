package com.sesample.tetris.game;

public class Board {

    public static final int PIECE_BLOCKS = 5;

    public static final int BOARD_WIDTH = 10;
    public static final int BOARD_HEIGHT = 22;

    public static final int POS_FREE = 0;

    private int[][] board;

    public Board() {
        initBoard();
    }

    public int[][] getBoard() {
        return board;
    }

    public boolean isFreeBlock(int pX, int pY) {
        return board[pX][pY] == POS_FREE;
    }

    /*sprawdza czy kawalek mozna przechowac w pozycji bez kolizji true jeśi tak false jesli nie
    * px-polozenie poziome w blokach,
    * py-polozenie pionowe w blokach
    * pPiece do rysowania
    * pRotation 1 z 4 mozliwych obrotow
    * */
    public boolean isPossibleMovement(int pX, int pY, int pPiece, int pRotation) {

        //kontrola kolizji juz zapisanymi na planszy, spawdzenie 5x5 bloków kawalka z odpowiednim obszarem w tablicy
        for (int i1 = pX, i2 = 0; i1 < pX + PIECE_BLOCKS; i1++, i2++) {
            for (int j1 = pY, j2 = 0; j1 < pY + PIECE_BLOCKS; j1++, j2++) {
                //sprawczenie czy kawalek jest poza tablica
                if (i1 < 0 ||
                        i1 > BOARD_WIDTH - 1 ||
                        j1 > BOARD_HEIGHT - 1) {
                    if (Piece.getBlockType(pPiece, pRotation, j2, i2) != 0)
                        return false;
                }

                //sprawdzenie czy kawalek ma kolizje z juz przechowywanym blokiem
                if (j1 >= 0) {
                    if ((Piece.getBlockType(pPiece, pRotation, j2, i2) != 0) &&
                            (!isFreeBlock(i1, j1)))
                        return false;
                }
            }
        }
        return true;
    }

    private boolean inBoardRange(int x, int y) {
        return x >= 0 && x < BOARD_WIDTH && y >= 0 && y < BOARD_HEIGHT;
    }

    public void storePiece(int pX, int pY, int pPiece, int pRotation) {
        // przechowanie kazdego bloczka do tablicy
        for (int i1 = pX, i2 = 0; i1 < pX + PIECE_BLOCKS; i1++, i2++) {
            for (int j1 = pY, j2 = 0; j1 < pY + PIECE_BLOCKS; j1++, j2++) {
                // przechowanie tylko bloczkow,ktore nie sa wadliwe
                if (Piece.getBlockType(pPiece, pRotation, j2, i2) != 0) {
                    if (inBoardRange(i1, j1)) {
                        board[i1][j1] = pPiece + 1;
                    }
                }
            }
        }
    }

    public int deletePossibleLines() {
        int deleted = 0;
        for (int j = 0; j < BOARD_HEIGHT; j++) {
            int i = 0;
            while (i < BOARD_WIDTH) {
                if (board[i][j] == POS_FREE) break;
                i++;
            }

            if (i == BOARD_WIDTH) {
                deleteLine(j);
                deleted++;
            }
        }
        return deleted;
    }

    public boolean isGameOver() {
        for (int i = 0; i < BOARD_WIDTH; i++) {
            if (board[i][0] != POS_FREE)
                return true;
        }
        return false;
    }

    public void initBoard() {
        board = new int[BOARD_WIDTH][BOARD_HEIGHT];
        for (int i = 0; i < BOARD_WIDTH; i++)
            for (int j = 0; j < BOARD_HEIGHT; j++)
                board[i][j] = POS_FREE;
    }

    public void deleteLine(int pY) {
        //przesuwanie wszytskich lini o jeden rzad
        for (int j = pY; j > 0; j--) {
            for (int i = 0; i < BOARD_WIDTH; i++) {
                board[i][j] = board[i][j - 1];
            }
        }
    }
}
