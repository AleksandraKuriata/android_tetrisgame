package com.sesample.tetris.game;

import android.os.SystemClock;

public class Timer {

    private long curTime;
    private long lastTime;
    private long deltaTime;

    public Timer() {
        lastTime = getTime();
        curTime = lastTime;
        deltaTime = 0;
    }

    public long getTime() {
        return SystemClock.uptimeMillis();
    }

    public long update() {
        curTime = getTime();
        deltaTime = curTime - lastTime;
        lastTime = getTime();

        return deltaTime;
    }

}
