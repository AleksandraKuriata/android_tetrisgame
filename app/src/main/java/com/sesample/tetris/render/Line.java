package com.sesample.tetris.render;

import android.content.Context;
import android.opengl.GLES20;

import com.sesample.tetris.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Line {
    private FloatBuffer VertexBuffer;

    private final int vertShaderResourceId = R.raw.line_vert;
    private final int fragShaderResourceId = R.raw.line_frag;

    protected int GlProgram;
    protected int PositionHandle;
    protected int ColorHandle;
    protected int MVPMatrixHandle;

    static final int COORDS_PER_VERTEX = 3;
    static float LineCoords[] = {
            0.0f, 0.0f, 0.0f,
            1.0f, 0.0f, 0.0f
    };

    private final int VertexCount = LineCoords.length / COORDS_PER_VERTEX;
    private final int VertexStride = COORDS_PER_VERTEX * 4;
    float color[] = { 1.0f, 1.0f, 1.0f, 1.0f };

    public Line(final Context context) {
        ByteBuffer bb = ByteBuffer.allocateDirect(
                LineCoords.length * 4);
        bb.order(ByteOrder.nativeOrder());

        VertexBuffer = bb.asFloatBuffer();
        VertexBuffer.put(LineCoords);
        VertexBuffer.position(0);

        int vertexShader = ResourceHelper.loadShader(context, GLES20.GL_VERTEX_SHADER, vertShaderResourceId);
        int fragmentShader = ResourceHelper.loadShader(context, GLES20.GL_FRAGMENT_SHADER, fragShaderResourceId);

        GlProgram = GLES20.glCreateProgram();
        GLES20.glAttachShader(GlProgram, vertexShader);
        GLES20.glAttachShader(GlProgram, fragmentShader);
        GLES20.glLinkProgram(GlProgram);
    }


    public void SetVerts(float v0, float v1, float v2, float v3, float v4, float v5) {
        LineCoords[0] = v0;
        LineCoords[1] = v1;
        LineCoords[2] = v2;
        LineCoords[3] = v3;
        LineCoords[4] = v4;
        LineCoords[5] = v5;

        VertexBuffer.put(LineCoords);
        VertexBuffer.position(0);
    }

    public void draw(float[] mvpMatrix) {
        GLES20.glUseProgram(GlProgram);

        PositionHandle = GLES20.glGetAttribLocation(GlProgram, "vPosition"); //opengl

        GLES20.glEnableVertexAttribArray(PositionHandle);

        GLES20.glVertexAttribPointer(PositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                VertexStride, VertexBuffer);

        ColorHandle = GLES20.glGetUniformLocation(GlProgram, "vColor");

        GLES20.glUniform4fv(ColorHandle, 1, color, 0);

        MVPMatrixHandle = GLES20.glGetUniformLocation(GlProgram, "uMVPMatrix");
        MyGLRenderer.checkGlError("glGetUniformLocation");

        GLES20.glUniformMatrix4fv(MVPMatrixHandle, 1, false, mvpMatrix, 0);
        MyGLRenderer.checkGlError("glUniformMatrix4fv");

        GLES20.glDrawArrays(GLES20.GL_LINES, 0, VertexCount);

        GLES20.glDisableVertexAttribArray(PositionHandle);
        MyGLRenderer.checkGlError("after line");
    }
}
