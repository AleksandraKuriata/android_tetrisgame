package com.sesample.tetris.render;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import com.sesample.tetris.R;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

public class Sprite {

    private final int vertShaderResourceId = R.raw.sprite_vert;
    private final int fragShaderResourceId = R.raw.sprite_frag;

    private final FloatBuffer vertexBuffer;
    private final FloatBuffer texCoordBuffer;

    private final int mProgram;
    private int mPositionHandle;
    private int mColorHandle;
    private int mMVPMatrixHandle;
    private int mTextureUniformHandle;
    private int mTextureCoordinateHandle;

    private float[] mVertex;
    private float[] mTexCoords;

    private int mTextureHandle;

    static final int COORDS_PER_VERTEX = 3;

    private final int vertexStride = COORDS_PER_VERTEX * 4;

    private float mColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };

    public Sprite(final Context context, int textureResourceId) {
        ByteBuffer bb = ByteBuffer.allocateDirect(
                3 * 6 * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();

        ByteBuffer tb = ByteBuffer.allocateDirect(6 * 2 * 4);
        tb.order(ByteOrder.nativeOrder());
        texCoordBuffer = tb.asFloatBuffer();

        int vertexShader = ResourceHelper.loadShader(context, GLES20.GL_VERTEX_SHADER, vertShaderResourceId);
        int fragmentShader = ResourceHelper.loadShader(context, GLES20.GL_FRAGMENT_SHADER, fragShaderResourceId);

        mProgram = GLES20.glCreateProgram();
        Log.d("glLink", "Success create shader: " + mProgram);

        GLES20.glAttachShader(mProgram, vertexShader);
        GLES20.glAttachShader(mProgram, fragmentShader);
        GLES20.glLinkProgram(mProgram);

        Log.d("glLink", "Success link shader: " + mProgram);


        mTextureHandle = ResourceHelper.loadTexture(context, textureResourceId);

        mVertex = new float[] {
                -0.5f,  0.5f, 0.0f,
                -0.5f, -0.5f, 0.0f,
                0.5f, -0.5f, 0.0f,
                -0.5f,  0.5f, 0.0f,
                0.5f, -0.5f, 0.0f,
                0.5f,  0.5f, 0.0f
        };

        mTexCoords = new float[] {
                0.0f, 0.0f,
                0.0f, 1.0f,
                1.0f, 1.0f,
                0.0f, 0.0f,
                1.0f, 1.0f,
                1.0f, 0.0f
        };
    }

    public void setVertex(float[] vertex) {
        mVertex = vertex;
    }

    public void setVertex(float top, float left, float bottom, float right) {
        mVertex = new float[] {
                left,  top, 0.0f,
                left, bottom, 0.0f,
                right, bottom, 0.0f,
                left,  top, 0.0f,
                right, bottom, 0.0f,
                right,  top, 0.0f
        };
    }

    public void setTexCoords(float[] texCoords) {
        mTexCoords = texCoords;
    }

    public void draw(float[] mvpMatrix) {
        vertexBuffer.clear();
        vertexBuffer.put(mVertex);
        vertexBuffer.position(0);

        texCoordBuffer.clear();
        texCoordBuffer.put(mTexCoords);
        texCoordBuffer.position(0);

        GLES20.glUseProgram(mProgram);

        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glEnableVertexAttribArray(mTextureCoordinateHandle);

        mPositionHandle = GLES20.glGetAttribLocation(mProgram, "vPosition");

        GLES20.glVertexAttribPointer(
                mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        mColorHandle = GLES20.glGetUniformLocation(mProgram, "vColor");

        GLES20.glUniform4fv(mColorHandle, 1, mColor, 0);

        mMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
        MyGLRenderer.checkGlError("glGetUniformLocation");

        GLES20.glUniformMatrix4fv(mMVPMatrixHandle, 1, false, mvpMatrix, 0);
        MyGLRenderer.checkGlError("glUniformMatrix4fv");

        mTextureUniformHandle = GLES20.glGetUniformLocation(mProgram, "u_Texture");
        mTextureCoordinateHandle = GLES20.glGetAttribLocation(mProgram, "a_TexCoordinate");

        GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, mTextureHandle);

        GLES20.glUniform1i(mTextureUniformHandle, 0);

        GLES20.glVertexAttribPointer(
                mPositionHandle, COORDS_PER_VERTEX,
                GLES20.GL_FLOAT, false,
                vertexStride, vertexBuffer);

        GLES20.glVertexAttribPointer(mTextureCoordinateHandle, 2, GLES20.GL_FLOAT, false,
                8, texCoordBuffer);

        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 6);

        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

}
